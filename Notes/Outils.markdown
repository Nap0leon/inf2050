# Les Outils de Build

## Mêmes étapes, pareil, compatible.

- Compiler le code source
- Compiler les classes de tests
- Exécuter les classes de tests
- Pacter le programme dans un format
    ```
    Exemple : .jar
    ```

Il est important que peu importe l'ordinateur, le build sera toujours le même.

### Confirmer la version du langage utilisé
Il est important dans le script de build afin d'informer la version du langage utilisé.



## **ANT**

Chaque `étape` du **buildlifecycle** est appelé `Target`

```xml
<target name="compile-test" depends="dependencies" description="Compile le code source applicatif">
    <mkdir dir="${build.dir}"/>
    <mkdir dir="${build.classes.dir}"/>
</target>
```

## **IVY**



## **MAVEN**

Maven possède `3 Lifecycle` 
1. Build LifeCycle
2. Clean LifeCycle
3. Site LifeCycle

Chaque `étapte` du **buildlifecycle** est appelé `Phase`

Un `goal` est une instance de classe.

Un `Mojo` est une classe.

### Lifecycle, phases, goals, plugins & bindings.
Les `goals` sont litérallement ce qui est exécuté.

Les `plugings` est un ensemble de **goals** déclaré dans le fichier `pom.xml`.

De base, `Maven` crée un repo **Local** et un repo **Maven**. 

Maven permet aussi d'avoir un `Repo d'entreprise`. Il est donc possible d'installer, par exemple, une version de librairie sur un repo centrale disponible à toute l'entreprise. 

**Exemple de fichier `xml` minimal**
```xml
<?xmlversion="1.0"encoding="utf-8"?>
    <project>
        <modelVersion>4.0.0</modelVersion>
        <groupId>ca.uqam.inf2050</groupId>
        <artifactId>hello-maven</artifactId>
        <version>0.1-SNAPSHOT</version>
    <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding> 
</properties>
 ```

**Exemple de dépendances**
 ```xml
<dependencies>
    <dependency>
        <groupId>commons-lang</groupId>
        <artifactId>commons-lang</artifactId>
        <version>2.0</version>
    </dependency>
</dependencies>
 ```

**Les Plugins**

On `rédéfini` ou `ajoute` des plugins.
```xml
<build> 
    <plugins>
        <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.0</version>
        <configuration>
            <release>11</release>
        </configuration>
    </plugin>
```

**Exécution des tests J-Unit**
```xml
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-api</artifactId>
    <version>5.3.2</version>
    <scope>test</scope> <!-- S'ignifique que cette dépendance est utilisée que pour les tests-->
</dependency>
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-engine</artifactId>
    <version>5.3.2</version>
    <scope>test</scope>
</dependency>

<plugin>
    <artifactId>maven-surefire-plugin</artifactId>
    <version>2.22.0</version>
</plugin>
```

### **Site Lifecycle**
Maven peut générer un site web par son site lifecycle.

```
mvn site
```

### **Notion d'écosystème**

`CheckStyle`: Regarde si le code est bien écrit, dans le sens "beau".

`PMD` : Fait une analyse sur la bonne syntaxe du code. Vérifie si il y a des choses pas correcte.

`Spotbugs` : Analyse des patterns de bug.

`JaCoCo` : Génère des tests et est capable de retourner quelles lignes ont été testées et quelles lignes n'ont pas été touchées par les tests.
- **Faire des tests**
    ```
    Il est important de lancer les tests avec Maven. Des rapports sont générés pour s'informer des erreurs possible. Très accurate en Java. 
    
    À Noter, une bonne `couverture de test` peut dépasser les 100% car plusieurs cas peuvent être répétés.
    ```


**Exemple de commande Maven**
```java
mvn clean test site

// Maven s'organise pour rouler chaque commande une après l'autre (clean, test, site)
```

### **GRADLE : MAVEN + ANT**

Gradle offre une plus grande flexibilité aux programmeurs en termes d'altérations de la séquence de tâches
 
**Exemple de code Gradle**
```groovy
plugins {
    id 'java'
}

sourceCompatibility = '1.11'
targetCompatibility = '1.11'
```
```

$ gradle compileJava
$ javadoc
```

**Dependancies**
```groovy
repositories {
    mavenCentral()
}

dependancies {
    compile 'commons-lang:commons-lang:2.0'
}
```

**Exécutions des tests JUNIT 5**
```groovy
test {
    useJUNITPlatform()
}

dependancies {
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.3.1'
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.3.1'
}
```
```
$ gradle test
$ open build/reports/tests/test/index.html
```

**Archive exécutable**
```groovy
jar {
    manifest {
        attributes 'Main-Class': 'Application'
    }
}
```
```
$ gradle jar
```

**ÜBER-JAR Exécutable**
```groovy
task uberJar(type: Jar) {
    manifest {
        attributes 'Main-Class': 'Application'
    }
    appendix = 'dependencies-all'
    from sourceSets.main.output
    dependsOn configurations.runtimeClasspath
    from {
        configurations.runtimeClasspath
            .findAll { it.name.endsWith('jar') }
            .collect { zipTree(it) }
    }
}
```
```
$ gradle uberJar
```

`l'avantage`: Entre les deux. Très grande flexibilité comme nous pouvons personnalité le **script de build**.

`le désavantage` : A aussi le pire des deux. Ce n'est plus Maven qui est responsable de faire le build. Si c'est **mon code** c'est à moi d'aller le débugger. Ce n'est plus automatique.


# Versionnage Scémantique

Changement de version majeur
- Code cassé, version `non rétrocompatible`.

Changement de version mineur
- Code a des ajouts mais `reste rétrocompatible`.

# Écosystème

Ensemble des outils et des pratiques utilisées par l'équipe de développement.
C'est `l'écosystème` qui gère les intéractions entre les membres de l'équipe, gère la façon dont le travail est effectué et permet les optimisation à l'échelle de l'entreprise.
- Quel outils ont mets en place ?
- Comment on utilise les outils ?

## Les normes de codifications
```
- utilitaire de gestion de sources
- utilitaire de construction automatisée
- système de billetterie
- système de documentation partagée
- système d'intégration continue
- éditeur de fichiers
- environnement de déploiement
- normes de codification
- procédures de revue de code
- procédures de déploiement
```

Le code **pusher** se formate neutre pour l'équipe.  Le code **puller** se formate selon vos normes de programmeurs personnelles.

**Il y a toujours un écosystème de développement en place**

`STANDARD` : Structuré, tout le monde est malheureux égal. Risque de créer des insatisfaction. **Par contre**, uniformisation des processus et temps de prise en main très rapide == ++++

`AD HOC`: C'est ça qui est ça. La collaboration devient chaotique et la prise en main ardue.
Dificile d'avoir des optimisations au sein de l'entreprise.
Cependant, **l'écosystème AD HOC** encourage l'innovation et les optimisations.

## Les principes pour équipes de petite taille
Être `tout seul ensemble`. 
- Peu de prérequis. Le plus petit dénominateur commun.
- Procédures d'interaction claires, précises, connues et documentées (p.e pull-request, billeterie, modèle de branches, déploiement, prise en main, etc).
- Tout ce qui peut être automatisé est automatisé.

##  Famille d'éditeur de texte

1. Les IDE (Visual studio, IntelliJ)
2. Éditeurs de texte pur/brute.

