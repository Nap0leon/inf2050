---
title: "Cours 8 & 9, Outils de développement"
author: [Maxime Masson]
titlepage: true
---
# GIT & Continuous Integration

## GIT

**Référence**

    Est un pointeur vers un commit. C'est un fichier texte se trouvant dans le repo qui contient le hash d'un commit.

**Branche**

    Est une référence mutable (Sa valeur peut changer dans le temps).

**Tag**

    Est une référence immuable vers un commit. Aussi une sorte `d'objet` dans GIT.

**Remote**

    Est une référence. Pointeurs vers le commit sur le repo distant lors du dernier téléchargement.

**Head** 

    Est une référence vers une autre référence. le `HEAD` peut aussi pointer directement vers un commit.

**Merge**

    Crée un nouveau commit avec une particularité : Il a un minimum de 2 parents. 

**Reglog**

    Affiche l'historique des commits
    Est une révision en git

**Rebase**

    Avec l'option `i`, permet de faire un rebase intéractif.
    Rebase = Déplacer le parent et merge en linéarisant tous les commits. 

**Cherry-Pick**

    Permet de sélectionner un commit d'une branche et de "rejouer" les modifications dans une autre branche.
    - Option -e et -x permettent de modifier le message du commit.

**Stash**

    Retire et sauvegarde les modifications en cours dans le working tree et dans l'index.
    Les modifications sont empilées dans un 4e espace appelé "stash"
    
    $git stash apply = Dépile les modifications sauvegardées et les appliquent à la branche en cours.
    - Peut créer des conflits.
    - Un stash seulement
    - Empile 1 à la fois
    - Dépile 1 à la fois
    - Aucun timeout

- git stash show --patch : Affiche les modifications appliquées
- git stash list : Affiche le contenu de la pile
- git stash clear : Supprime la totalité de la pile

<br>

### Procedures
---

- Le travail en cours d'un programmeur ne doit pas être entravé par le travail d'un autre
- Le travail réalisé par un programmeur doit être inspecté par un autre
- L'équipe de développement ne doit pas entraver l'équipe assurance qualité
- L'équipe assurance qualité ne doit pas entraver l'équipe d'exploitation
- Il est possible d'avoir complété des fonctionnalités mais de ne pas les mettre en production
- Le code source a plusieurs versions concurrentes déployées en production.
- Le code source déployé en production est légèrement différent du code source développé et testé

<br>

### Git Flow
---

Un des premier workflows pour git documentés.
- Se concentre sur des préoccupations reliées au développement actif, la maintenance continue et la maintenance d'urgence.

#### Préparer une livraison

Lorsqu'un ensemble de fonctionnalités est complété, une branche de "livraison" est créé à partir de *develop* (release branch).
- Seuls des correctifs seront commités
- La branche de livraison est fusionnée vers develop (branche de production) afin d'intégrés les correctifs 

<br>

### GITHUB Flow
---

#### 1 Repo central & Repos contribution

#### 1 Seule Branche Permanente

#### Fusion via Pull-Requests

|Avantages|Inconvénient|
|---|---|
|Parfait pour le développement open-source|--|
|Plutôt simple|--|

<br>

### GITLAB Flow
---

Prend de l'ampleur après l'achat de Github par Microsoft.

#### 2 Branches permanentes
- Une branche *Master*
- Une branche *Production*

#### 1 Branche permanente par environnement
- Représente l'environnement de pipeline de livraison.

#### 1 Branche stable par livraison
- Seuls les correctifs de bugs majeurs sont commités dans ces branches.
- Privilégie la correction de bugs dans MASTER puis le cherry-picking

|Avantages|Inconvénient|
|---|---|
|Penser pour projet a branche stable|Compliqué pour des modes de développement simple|
|Incorpore plusieurs bons conseil|Se fie beaucoup sur le cherry-picking pour partager des changements entre branches.|


### Faire des commit
---

Un commit crée un objet `Commit` et change la valeur de la branche en cours (Celle pointé par HEAD).

```git
$ git commit -m "Message";
```

<br>

### Qu'est-ce qu'un TAG ?

Un tag est un objet contenant de l'information supplémentaire (Horodatage, auteur, message, hash d'un commit) ainsi qu'une reference vers cet objet.

- Permet d'identifier un commit de façon stable.
- Signet permanent à un commit particulier.

<br>

### Comment Merge ?

On utilise la comment `$ git merge <branche>`. Merge le branche visé sur celle courante.

On crée un objet commit (Merge commit). La particularité : Il a plus de 1 parent.

- Représente l'unification des historiques parallèles.
- "Fast-Forward" peut être créé lorsque tous les commits d'une des deux branches sont des ancêtres de l'autre 

<br>

### Supprimer une branche ?

On utilise la commande `$ git branch -d <branche>` pour supprimer la référence. 
- ***Attention*** : Supprimer une branche peut faire en sorte que des commits deviennent non joignables. Les commits *perdus* seront éventuellement **supprimés**.

<br>

### Afficher l'historique

Git conserve un historique de tous les commits. Pour afficher les commits d'une branche, on utilise la commande `$ git reflog`.

### Rebase

La première règle du `Rebase` : On ne fait pas de rebase sur une branche **partagée**. Seulement sur les *branches locales*.

`$ git rebase <bracnch>` crée des nouveaux commits et risque de casser les références. 

Permet de prendre des commits qui existait, calculer les modifications que les commits faisait et finalement de rejouer les modifications sur un nouveau commit.

```git
$ git rebase -i <branch>
```
- Permet de faire un rebase interactif
- Une interface permet alors de sélectionner les commits qui feront partie du rebase (`pick` ou `drop`), d'ordonanncer les commits différemment, de modifier les commits (`edit` ou `reword`) ou même

***Exemple***
```git
$ git rebase -i master

pick 2uc7
drop 3zu6
fixup 43d9

-> Crée un nouveau commit #### qui contient pick 2uc7 & fixup 43d9
```

<br>

## CI - Continuous Integration

L'intégration continue fait partie du principe `Agile`. C'est une façon de régler les erreurs croisées, soit les erreurs causées par la combinaisons des modifications.

### Processus

1. À intervalle réguliers et fréquents, le programme fait un `git pull` sur le repo principal et une branche en particulier.

2. Si des commits ont été faits depuis la dernière exécution : 
    
    - Le programme déclenche la construction du programme et l'exécution des tests
    - Si des erreurs de compilation ou d'exécution sont détectés : 
        - Le programme avise l'équipe

### Prérequis

1. Gestionnaire de source unique
2. Build automatisé et standardisé
3. Suite de test complète
4. Tous les commits dans "Master"

    Les commits de l'équipe doivent se retrouver dans une branche et un repo connu.  De cette façon, le processus d'intégration interrogera cette branche à `intervalle régulier`.

5. Garder le processus rapide

    Le but est de détecter rapidement les erreurs de combinaison. Un processus **étagé** peut aider.

6. Corriger les erreurs
    
    Toute l'équipe doit être avisée lorsque le build est cassé.

***Quelques exemples de CI***
- Jenkins
- TravisCI
- Bamboo
- TeamCity
- CruiseControl
- Script bash, crontab...

<br>

### Que peut-on faire de plus ?
---

L'étape suivante est `d'automatiser` le processus de livraison du système.

**Étage1**

Compilation des tests unitaires rapide automatisés. Correspond au processus d'intégration continue. Un programme obtient les sources et produit un artefact référence et des rapports.

**Étage 2**

Obtient l'Artefact référence, une configuration environnementale puis exécute un ensemble de tests d'acceptation automatisés.

**Étage 3**

La production.

### Implication

- Toutes les tâches pouvait être automatisés le sont.
- Les artefacts produits reçoivent les paramètres de configuration environnementaux.
- Déployé peut se faire automatiquement
- L'approvisionnement / créations des serveurs est automatisés
- L'exécution de la `majorité` des tests est automatisée.
- Tout est sous gestion de source.

### Avantages

    Haut degré de confiance envers la capacité de livrer le système. 
    Accélère la boucle de rétroaction avec le client.
    Force l'application de bonnes pratiques
    Réduction du risque

### Inconvénients

    Modifications au système sont peut-être nécessaires
    Prérequis à la mise en place du processus sont nombreux


### LEAN STARTUP
---

Utiliser l'environnement de production et les utilisateurs comme un environnement de tests.

**Implications**

- Processus de livraison continue est en place.
- Les tests faisant partie du pipeline de déploiement sont entièrement automatisés (Et offre maximum de confiance).
- La culture de l'entreprise et de l'quipe de développement permet ce type d'opération.
- L'architecture de déploiement permet se type d'opération.


<br>

### DEVOPS
---

Équipe de développement qui assure la survie de l'organisation en `améliorant constamment les logiciels` pour que ceux-ci restent pertinents pour les clients.

Avoir une équipe unifié où certains font du développement et certains font des opérations sans `être concurent`.

1. La première voie
    
    L'emphase mise sur la chaine de valeurs produite par les services informatiques.

2. La deuxième voie

3. La troisième voie

### INFRASTRUCTURE AS CODE

- **Virtualisation** : Docker, Vagrant, Packer...
- **Orchestration de conteneurs** (cluster) : Kubernetes, Docker Swarm, Mesos..
- **Orchestration de conteneurs** (dev) : Docker Compose, Docker Stack...
- **Gestion de configuration** : Ansible, Chef, Puppet, Saltstack...
- **Outils d'orchestration** : Docker Machine, Terraform...

<br>

## Revue de Code

Une revue de code permet d'éliminer plusieurs erreurs.  Elle permet aussi d'apprendre.

Si on réalise une revue de code dans un environnement toxique, ça peut dégénérer rapidement.

```
Une revue de code réussie nécessite de l'ÉCOUTE, une couverture d'esprit, de l'HUMILITÉ, du RESPECT et de l'EMPATHIE à la fois de la poart des évaluateurs et des auters.
```

### Performance

- Lors de connexion aux ressources externes, comment sont gérés les *timeouts* ?
- Le code utilise-t-il les techniques de méta-programmation judicieusement ?
- Oppotunités pour utiliser le parallélisme ou des appels asynchrones ?
- Le code est-il *threadsafe* ? Des conditions de course sont-elles possibles ?
- Interface des structures de données choisie en fonction de leur utilisation ?
- Implémentation des structures de données est-elle choisie en fonction de leur utilisation ?
- Trop d'itérations ou de tris de listes ?
- Ordre d'algorithme ?

