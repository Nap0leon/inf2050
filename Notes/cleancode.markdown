# Clean Code

## Crash Early

`Garbage in, **nothing** out`. Si un comportement ou une sisutation est jugées "impossible", ajouter des assertions (tests).

## Keep Knowlegde in Plain Text.

Lorsque la connaissance est conservée en `format texte`, il devient impossible d'utiliser des outils de manipulation de texte.
Il faut donc la conservée en `Plain Text`.

## "Select" isn't broken

Le déboggage peut être une activité rigoureuse.  

- Premièrement, trouver comment reproduire l'erreur de façon fiable.
- Deuxièmement, procéder à l'élimination des variables.
- Finalement, poser une hypothèse ET concevoir/réaliser une expérience qui la validera ou non.

## Evil Wizards

Le code généré doit être entièrement compris avant d'être utilisé dans un projet

## Minimize coupling between modules

La `Loi de Demeter` permet de minimiser le couplage : Le code d'une méthode peut appeler...
1. Les méthodes du même objet
2. Les méthodes des objets passés en paramètre
3. Les méthodes des objets créés
4. Les méthodes des variables d'instance

## Design Using Services

Les instances d'un module facile à maintenir est à tester sont habituellement immuables, référentiellement transparent, indépendants, facilement parallélisables et définis par des contrats.

## Don't program by coincidence

Toujours rester CONSCIENT DU CODE QU'ON ÉCRIT.
- Connaitre ce qu'il y a a faire
- Connaitre les plans
- Etc.

## Invest REGURLARLY in your knowledge Portfolio

Toujours tenir sont portfolio à jour.
- Lire des manuels techniques
- Apprendre des nouveaux langages de programmation
- Participer à des `user-groups`
- Suivre des formations

## Choisir un identifiant

- Devraient toujours révéler l'intention du programmeur
- Éviter les blagues
- Rester uniforme dans le choix des identifiants, l'utilisation du pluriel et le choix de langue
- la longueur devrait être lié à la VISIBILITÉ de la chose nommée
- l'identifiant d'une classe devrait être un nom et faire référence à sa responsabilité dans le système
- Utiliser les termes du domaine du problème
- un methode devrait être un verbe

## Écrire une méthode

- Une méthode doit faire que UNE action et être nommée par cette action
- Une méthode peut déléguer le traitement à d'autre méthodes
- Les méthodes doivent être ordonné dans le sens des appels, de haut en bas
- Une méthode ne doit pas avoir plus de 3 paramètres
- Éviter les effets secondaires
- Une Methode pose une action OU obtient de l'information

## Comment écrire un bon commentaire

- Expliquer l'intention du programmeur
- Donner de l'information utile
- CLARIFIER le fonctionne d'une algo
- Ne doit pas être redondant
- Ne doit pas induire le lecteur en erreur
- Ne doit pas remplacer un bon identifiant
- Ne doit pas remplacé une fonctionnalité de l'éditeur/langage
- Ne doit pas commenté du code source





