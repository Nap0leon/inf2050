# Note & Revision livrable 3

Principalement de l'intégration

## Étape 1

Changement de la syntaxe du code. 
Utilisation de JSON car il existe déjà une librairie qui permet de transformer un objet JSON en objet JAVA.

## Étape 2

Minischeme web.
Application Web avec 2 fenêtre (Une qui prend le code source minischeme et une pour output le resultat).
- Lire le code source existant
- Modifier le code adéquatement (Nouvelle syntaxe)
- Ajouter le code dans l'interface

Instruction sont sur un repo.

## Étape 3

Nous aurons 3 modules au final.  
1. Parser
2. Minischeme web
3. Artefact/build Java pour le Parser & Langage. -- A FAIRE

    Comment lier ?
    Possible de lier que par le code source (Même structure de répertoire).
    Possible d'utiliser Maven qui offre des sous-projets (proejt enfant pour parser et projet enfant pour langage)
    Possible d'utiliser Gradle ** ** **

***Intégration***
Doit mettre une dépendance du projet web vers le module (Évaluateur + compilateur). Cette dépendance doit être ajouté dans le module "web".

## Intégration Continue

Prendre le module (évaluateur + compilateur) et faire de l'intégration continue dessus.
- Installer sur notre poste un serveur d'intégration continue (Ex : Jenkins). // Image docker ?
- Configurer le serveur Jenkins pour performer l'intégration continue.

## Tests UI

Utiliser un touils de tests d'interfaces web (Selenium, Cypress, CasperJS ou autre)
- Programmer une suite de tests fonctionnels pour l'app web.
