# OUTILS ET PRATIQUES DE DEVELOPPEMENT LOGICIEL

Cours d'utilisation d'outils et de pratique de développement logiciel dans un environnement professionnel. Axé sur la gestion de source ainsi que 
les outils de builds.

## Contenu du cours

- Comparaison des modèles de développement traditionnels et des processus de développements modernes.
- Développement logiciel dans un contexte de logiciel libre (`open source`).
- Étude de cas
- Compréhension de tests (JUNIT)
- Gestion de la configuration
- Construction automatisée (Builds)
- Environnement intégré de développement
- Outils de pistage et de revue de code

## Laboratoires & Démonstrations

Les laboratoires du cours ce situe au lien suivant
[Laboratoire](https://github.com/Nouninoun/INF2050_Laboratoires)


